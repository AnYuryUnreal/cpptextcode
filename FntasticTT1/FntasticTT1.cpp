﻿#include <iostream>
#include <cctype> //Нужна для перевода в строчные
struct SymbolMap //Структура карты частоты повторения символов
{	char Sym;
	int Freq;
};
class CharOp
{
	char ProcessingText[255]; //Текст для переработки. Ограничен 255 символами. Поскольку обработка более длинных строк маловероятна
	//и не требуется в задании - защита от перегрузки массива возложена на внутренние инструменты функции getline;
	SymbolMap CharFreq[255]; //Карта частоты повторения символов. На всякий случай равна по длине вводимой строке.
	public:
	CharOp() //Конструктор зануляющий массивы
	{
		for (int i = 0; i < 255; i++)
		{
			ProcessingText[i] = NULL;
			CharFreq[i].Sym = NULL;
			CharFreq[i].Freq = NULL;
		}
	};
	void GetText() { //Процедура считывания ввода пользователя
		std::cout<<"Enter input string" << std::endl;
		std::cin.getline(ProcessingText,255);
		
		for (int i = 0; i < 255; i++)
		{
			if (ProcessingText[i] != NULL)
				ProcessingText[i] = tolower(ProcessingText[i]); //Перевод в строчные
			else
				break;
		}
	};

	int FindSymbol (char Symbol) //Поиск символа в карте частоты повторения символов, если символ не найден вернётся значение "-1"
	{
		int SymbolId = -1;
		
		for (int i = 0; i < 255; i++)
		{
			if (CharFreq[i].Sym == NULL)
				break;

			if (Symbol == CharFreq[i].Sym)
			{
				SymbolId = i;
				break;
			}

			
		};
		return SymbolId;
	};
	void StoreSymbol(char Symbol) //Запись нового символа в карту на первую пустую позицию
	{
		for (int i = 0; i < 255; i++)
		{
			if (CharFreq[i].Sym == NULL)
			{
				CharFreq[i].Sym = Symbol;
				CharFreq[i].Freq = 1;
				break;
			}

		}
	};

	void CountSymbols()  //Подсчёт частоты повторения символов
	{

		for (int i = 0; i < 255; i++)
		{
			if (ProcessingText[i] != NULL)
				if (FindSymbol(ProcessingText[i]) != -1)
					CharFreq[FindSymbol(ProcessingText[i])].Freq++;
				else
					StoreSymbol(ProcessingText[i]);
			else
				break;


		}
	};

	void ReforgeText() // Переработка введённого текста в требуемый формат
	{
		CountSymbols();
		for (int i = 0; i < 255; i++)
		{
			if (ProcessingText[i] != NULL)
				if (CharFreq[FindSymbol(ProcessingText[i])].Freq == 1)
					ProcessingText[i] = '(';
				else
					ProcessingText[i] = ')';
			else
				break;

		}
	};

	void PrintText() //Вывод текста в консоль
	{
		for (int i = 0; i < 255; i++)
		{
			if (ProcessingText[i] != NULL)
				std::cout << ProcessingText[i];
			else
				break;
		}
		std::cout << std::endl;//Gthtyjc cnhjrb
	};
	
};

int main()
{
	CharOp UserInput;
	UserInput.GetText();
	UserInput.ReforgeText();
	UserInput.PrintText();
	system ("Pause");
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"